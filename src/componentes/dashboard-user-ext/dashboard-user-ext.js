import React, { Component } from 'react'
import Header from '../header/Header'
import LeftMenu from '../LeftMenu/LeftMenu'
import { Grid } from '@material-ui/core'

export default class DashboardUserExt extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <Grid container spacing={1} style={{ width: '100%' }}>
                    <Grid item xs={2} style={{ borderRight: 'solid 2px #dddddd', height: '1426px' }}>
                        <LeftMenu />
                    </Grid>
                    <Grid item xs={10}>
                        <h1 style={{ textAlign: 'center' }}>Bienvenido al dashboard externo</h1>
                    </Grid></Grid>

            </React.Fragment>
        )
    }
}
