import React from "react";
import { useStateValue } from "../../context/store";
import { Route, Redirect } from "react-router-dom";

function SecureRoute({ component: Component, ...rest }) {
  const [{ sessionUser }, dispatch] = useStateValue();
  console.log(sessionUser);
  return (
    <Route
      {...rest}
      render={(props) =>
        sessionUser ? (
          sessionUser.authenticated === true ? (
            <Component {...props} {...rest} />
          ) : <Redirect to="/auth/login" />
        ) : <Redirect to="/auth/login" />
      }
    />
  );
}

export default SecureRoute;
