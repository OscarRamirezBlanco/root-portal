import React, { useState } from "react";
import { Container, Typography, Button, Grid, TextField, FormControlLabel, Checkbox, FormControl, Link } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { loginuser } from "../../actions/user-actions";
import { useStateValue } from "../../context/store";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  margin: {
    marginBottom: "15px",
    width: "100%",
  },
}));

const style = {
  paper: {
    marginTop: 8,
    display: "flex",
    flexDirection: "column",
    align: "center",
  },
  form: {
    width: "100%",
    marginTop: 20,
  },
  submit: {
    textTransform: "none",
  },
  leftside: {
    backgroundColor: "#004B93",
    width: "100%",
    height: "100%",
    marginTop: 8,
    display: "flex",
    flexDirection: "column",
    align: "center",
  },
};

const Login = (props) => {
  const classes = useStyles();
  const [{ sessionUser }, dispatch] = useStateValue();

  const [usuario, setUsuario] = useState({
    username: "user1",
    password: "Usuario1*",
  });

  const addValuesToMemory = (e) => {
    const { name, value } = e.target;
    setUsuario((anterior) => ({
      ...anterior,
      [name]: value,
    }));
  };

  const loginUserBtn = (e) => {
    e.preventDefault();
    loginuser(usuario, dispatch).then((response) => {
      if (response.status === 200) {
        window.localStorage.setItem(
          "token_app",
          response.data != null && response.data.access_token
        );
        props.history.push("/dashboard");
      }

    }, error => {

      console.log(error);
      // alert(error.error_description)

      
        if (error.error_description !== undefined) {
          dispatch({
            type: "OPEN_SNACKBAR",
            openMessage: {
              open: true,
              message: error.error_description,
            },
          });
        }
        else if (error.status === 500) {
          dispatch({
            type: "OPEN_SNACKBAR",
            openMessage: {
              open: true,
              message: "Ocurrió  un error en el servicio de autenticación",
            },
          });
        } else {
          dispatch({
            type: "OPEN_SNACKBAR",
            openMessage: {
              open: true,
              message: "Las credenciales del usuario son incorrectas",
            },
          });
        }

    });
  };
  return (
    <Grid container spacing={0} style={{ height: "100%", margin: 0 }}>
      <Grid item xs={12} sm={6} md={6}>
        <Container maxWidth="xs" justify="center">
          <div style={style.paper}>
            <Typography
              component="h1"
              variant="h5"
              style={{ color: "#124387", marginTop: 100 }}
            >
              <b>Identifícate en Mutual SER</b>
            </Typography>
            <form style={style.form}>
              <FormControl className={classes.margin}>
                <TextField
                  value={usuario.username}
                  onChange={addValuesToMemory}
                  variant="outlined"
                  label="Usuario o correo electrónico"
                  name="username"
                  fullWidth
                  margin="normal"
                />
              </FormControl>
              <FormControl className={classes.margin}>
                <TextField
                  value={usuario.password}
                  onChange={addValuesToMemory}
                  variant="outlined"
                  type="password"
                  label="Contraseña"
                  name="password"
                  fullWidth
                  margin="normal"
                />
              </FormControl>
              <Grid container className={classes.margin}>
                <Grid item xs>
                  <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label="Recordarme"
                  />
                </Grid>
                <Grid item style={{ paddingTop: "10px" }}>
                  <Link href="#" variant="body2">
                    OLVIDÉ MI CLAVE
                </Link>
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                style={style.submit}
                onClick={loginUserBtn}

              >
                INGRESAR
              </Button>
            </form>
          </div>
        </Container>
      </Grid>
      <Grid item xs={12} sm={6} md={6} justify='center'>
        <div style={{
          marginTop: '20%',
          color: 'white',
          fontSize: '16px',
          padding: '16px 32px',
          position: 'absolute',
          zIndex: 4
        }}>
          <Container maxWidth="xs" justify="center">
            <Typography
              variant="h3" gutterBottom>
              Bienvenido,
                  </Typography>
            <Typography
              component="h1"
              variant="h5"
              style={{ color: "#ffffff" }}
            >
              ¿Qué necesitas hacer hoy?
            </Typography>
          </Container>
        </div>
        <div style={{ position: 'relative' }}>
          <img src="/nurse.PNG" style={{ width: '100%', height: 'auto' }} />
          <div style={{
            position: 'absolute', /* Sit on top of the page content */
            display: 'block', /* Hidden by default */
            width: '100%', /* Full width (cover the whole page) */
            height: '100%', /* Full height (cover the whole page) */
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            background: 'linear-gradient(to bottom left, #3333cc 0%, #6699ff 100%)',
            opacity: 0.6,
            zIndex: 2, /* Specify a stack order in case you're using a different order for other elements */
          }}>

          </div>
        </div>
      </Grid>
    </Grid>
  );
};

export default withRouter(Login);
