import React, { Component } from 'react'
import DashboardUserInt from '../dashboard-user-int/dashboard-user-int'
import DashboardUserExt from '../dashboard-user-ext/dashboard-user-ext';

export default class MainDashboard extends Component {
    render() {
        const typeUser = 1;
        return (
            <React.Fragment>
                {
                    typeUser === 0 ? <DashboardUserInt /> : <DashboardUserExt />
                }
            </React.Fragment>
        )
    }
}
