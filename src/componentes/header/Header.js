import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import ExitToAppOutlined from '@material-ui/icons/ExitToAppOutlined';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { useStateValue } from "../../context/store";
import { withRouter } from 'react-router-dom';



const useStyles = makeStyles((theme) => ({
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        width: '100%',
        backgroundColor: '#013F8C',
        height: '64px',

    },
    toolbarTitle: {
        flex: 1,
        color: '#ffffff',
        paddingLeft: '20px',
        fontSize: '20px'
    },
    toolbarSecondary: {
        justifyContent: 'space-between',
        overflowX: 'auto',
    },
    toolbarLink: {
        padding: theme.spacing(1),
        flexShrink: 0,
    },
    iconsColor: {
        color: "#fff"
    }
}));

const  Header=(props)=> {
    const classes = useStyles();
    const [{ sessionUser }, dispatch] = useStateValue();

    return (
        <React.Fragment>
            <Toolbar className={classes.toolbar}>
                <svg width="50" height="25" viewBox="0 0 50 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path d="M25.0004 5.54391C26.5346 5.54391 27.7782 4.30286 27.7782 2.77195C27.7782 1.24105 26.5346 0 25.0004 0C23.4663 0 22.2227 1.24105 22.2227 2.77195C22.2227 4.30286 23.4663 5.54391 25.0004 5.54391Z" fill="white" />
                        <path d="M0 18.8899L4.81988 18.5901C4.92426 19.3672 5.13673 19.9593 5.45731 20.3664C5.97919 21.025 6.72472 21.3544 7.69392 21.3544C8.41709 21.3544 8.97437 21.186 9.36578 20.8493C9.75719 20.5125 9.95289 20.1221 9.95289 19.6781C9.95289 19.2562 9.76651 18.8788 9.39374 18.5457C9.02097 18.2127 8.15616 17.8981 6.79928 17.6021C4.57757 17.1062 2.99333 16.4476 2.04649 15.6261C1.0922 14.8046 0.615066 13.7573 0.615066 12.4844C0.615066 11.6481 0.859226 10.858 1.34755 10.1142C1.83588 9.37042 2.57023 8.78576 3.55061 8.3602C4.53099 7.93464 5.8748 7.72187 7.58209 7.72187C9.67705 7.72187 11.2743 8.10857 12.374 8.88197C13.4737 9.65537 14.1279 10.8858 14.3366 12.5732L9.56148 12.8507C9.43474 12.118 9.16821 11.5852 8.7619 11.2521C8.35558 10.9191 7.79457 10.7526 7.07885 10.7526C6.48988 10.7526 6.04629 10.8765 5.74807 11.1244C5.44986 11.3724 5.30075 11.674 5.30075 12.0292C5.30075 12.2882 5.42376 12.5214 5.66979 12.7286C5.90836 12.9432 6.47496 13.1431 7.36961 13.3281C9.58386 13.8017 11.17 14.2809 12.128 14.7657C13.086 15.2505 13.7831 15.8518 14.2192 16.5697C14.6553 17.2876 14.8734 18.0906 14.8734 18.9787C14.8734 20.0222 14.5827 20.9843 14.0011 21.865C13.4196 22.7458 12.607 23.4137 11.5632 23.8689C10.5195 24.324 9.20363 24.5516 7.61564 24.5516C4.82732 24.5516 2.89641 24.0187 1.82283 22.953C0.749257 21.8872 0.141652 20.5329 0 18.8899ZM17.4051 8.19373H30.9259V11.6527H22.4614V14.2275H30.3133V17.5317H22.4614V20.7254H31.1709V24.3943H17.4051V8.19373ZM34.019 24.3943V8.19373H42.3863C43.9379 8.19373 45.1237 8.32634 45.9438 8.59156C46.7639 8.85678 47.4252 9.34854 47.9276 10.0669C48.43 10.7852 48.6812 11.66 48.6812 12.6914C48.6812 13.5902 48.4891 14.3656 48.1049 15.0176C47.7207 15.6696 47.1924 16.1982 46.5201 16.6034C46.0916 16.8613 45.5042 17.0749 44.758 17.2444C45.3564 17.4433 45.7923 17.6422 46.0657 17.8411C46.2504 17.9737 46.5182 18.2574 46.8692 18.6921C47.2201 19.1267 47.4547 19.4619 47.5729 19.6977L50 24.3943H44.3257L41.6438 19.4435C41.3039 18.8026 41.001 18.3863 40.735 18.1948C40.373 17.9443 39.9629 17.819 39.5048 17.819H39.0615V24.3943H34.019ZM39.0823 14.6425H41.2044C41.434 14.6425 41.8784 14.5686 42.5376 14.4208C42.8709 14.3543 43.1431 14.1844 43.3542 13.911C43.5653 13.6376 43.6709 13.3235 43.6709 12.9688C43.6709 12.4442 43.5042 12.0415 43.1709 11.7607C42.8376 11.4799 42.2117 11.3395 41.2933 11.3395H39.0823V14.6425Z" fill="white" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="50" height="24.5902" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                <Typography
                    component="h2"
                    variant="h5"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.toolbarTitle}
                >
                    Mutual SER
                </Typography>
                <IconButton className={classes.iconsColor}>
                    <NotificationsNoneIcon />
                </IconButton>
                <IconButton className={classes.iconsColor}>
                    <MailOutlineIcon />
                </IconButton>
                <IconButton className={classes.iconsColor} onClick={() => {
                    dispatch({
                        type: "LOGOUT",
                        session: {},
                        authenticated: false
                    });
                    window.localStorage.removeItem("token_app");
                    props.history.push("/auth/login");

                }}>
                    <ExitToAppOutlined />
                </IconButton>
            </Toolbar>
            {/* <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>

            </Toolbar> */}
        </React.Fragment>
    );
}

Header.propTypes = {
    sections: PropTypes.array,
    title: PropTypes.string,
};

export default withRouter(Header);
