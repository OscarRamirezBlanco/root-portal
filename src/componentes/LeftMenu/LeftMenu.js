import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Divider, ListItem, ListItemIcon, ListItemText, Button } from '@material-ui/core';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },

    cardUser: {
        width: '100%',
        height: '156px',
        left: '79px',
        top: '88px',
        margin: 'auto',
        paddingTop: '30px'
    },
    cardUserText: {
        margin: 'auto',
        width: '112px',
        height: '40px',
        left: '79px',
        top: '204px'
    },
    cardUserTextName: {
        margin: 'auto',
        height: '20px',
        left: '6.7%',
        right: '6.7%',
        top: 'calc(50% - 20px/2 - 10px)',

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '16px',
        lineHeight: '20px',
        /* identical to box height, or 125% */

        textAlign: 'center',
        letterSpacing: '-0.05px',

        color: '#3A3B3F'
    },
    cardUserTextTitle: {
        margin: 'auto',
        height: '16px',
        left: '7.14%',
        right: '8.93%',
        top: 'calc(50% - 16px/2 + 12px)',

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '12px',
        lineHeight: '16px',
        /* identical to box height, or 133% */

        textAlign: 'center',

        color: '#9EA0A5'
    },
    large: {
        width: '100px',
        height: '100px',
        margin: 'auto'
    },
    ripsButton: {
        backgroundColor: '#47B881',
        //textAlign: 'center',
        margin: 'auto',
        marginTop: '30px',
        marginLeft: '10px',
        padding: 'auto'
    },
    textMenu: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 500,
        fontSize: '14px',
        lineHeight: '20px',
        /* identical to box height, or 143% */

        letterSpacing: '-0.05px',

        color: '#66788A'
    }
}));

function LeftMenu(props) {
    const classes = useStyles();

    return (
        <div style={{ width: '100%' }}>
            <div style={{ width: '100%' }}>
                <div className={classes.cardUser}>
                    <Avatar
                        src={require(`../../images/user1.jpg`)}
                        className={classes.large}
                    />
                    <div className={classes.cardUserText}>
                        <div className={classes.cardUserTextName}>Luisa Moreno</div>
                        <div className={classes.cardUserTextTitle}>Empleado Directo</div>
                    </div>
                </div>
            </div>
            <Divider />
            <Button variant="contained"
                color="secondary"
                className={classes.ripsButton}
                startIcon={<AttachFileIcon />}
            >
                CARGAR ARCHIVOS RIPS
            </Button>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Aplicaciones" />
            </ListItem>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Modulos" />
            </ListItem>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Funcionalidades" />
            </ListItem>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Operaciones" />
            </ListItem>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Organizaciones" />
            </ListItem>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Administracion de Seguridad" />
            </ListItem>
            <Divider />
            <p className={classes.textMenu}>
                Soporte
            </p>
            <ListItem button>
                <ListItemIcon> <InboxIcon /> </ListItemIcon>
                <ListItemText className={classes.textMenu} primary="Ayuda" />
            </ListItem>
        </div>
    );
}

export default LeftMenu;