import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme/theme";
import Login from "./componentes/seguridad/Login";
import RegistrarUsuario from "./componentes/seguridad/Registrar";
import { Grid, Snackbar } from "@material-ui/core";
import MainDashboard from "./componentes/dashboad/main-dashboad";
import { useStateValue } from "./context/store";
import SecureRoute from "./componentes/seguridad/secure-route";

function App() {

  const [{ sessionUser,openSnackbar }, dispatch] = useStateValue();
  console.log(sessionUser);
  return (
    <React.Fragment>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openSnackbar ? openSnackbar.open : false}
        autoHideDuration={3000}
        ContentProps={{ "aria-describedby": "message-id" }}
        message={
          <span id="message-id">
            {openSnackbar ? openSnackbar.message : ""}
          </span>
        }
        onClose={() =>
          dispatch({
            type: "OPEN_SNACKBAR",
            openMessage: {
              open: false,
              message: "",
            },
          })
        }
      ></Snackbar>
    <Router>
      <MuiThemeProvider theme={theme}>
        <Grid container>
          <Switch>
            <Route exact path="/auth/login" component={ Login} />
            <Route exact path="/auth/registrar" component={RegistrarUsuario} />
            <SecureRoute exact path="/" component={MainDashboard} />
            <SecureRoute exact path="/dashboard" component={MainDashboard} />
            {/* {              
              sessionUser && sessionUser.authenticated === true ?
                <Route exact path="/" component={MainDashboard} />
                :
                <Route exact path="/" component={Login} />
            } */}
          </Switch>
        </Grid>
      </MuiThemeProvider>
    </Router>
    </React.Fragment>

  );
}

export default App;
