import axios from 'axios';

//axios.defaults.baseURL = 'http://zonaser-auth-demo.appspot.com/api_rest/';

axios.interceptors.request.use((config) => {
  const token_app = window.localStorage.getItem('token_app');
  if(token_app) {
      config.headers.Authorization = `Bearer ${token_app}`;
      return config;
  }
  return config;

}, error => {
  return Promise.reject(error);
});

const requestGeneric = {
    get : (url) => axios.get(url),
    post : (url, body, headers) => axios.post(url, body, headers),
    put : (url, body) => axios.put(url, body),
    delete : (url) => axios.delete(url),
    // postForm: (url,body, headers)=>{
    //   console.log(url);
    //   console.log(body);
    //   console.log(headers);
    // }
};


export default requestGeneric;
