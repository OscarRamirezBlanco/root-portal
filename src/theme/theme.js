import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#63a4fff",
      main: "#538DFF",
      dark: "#124387",
      contrastText: "#DFEAFF",
    },
  },
});

export default theme;
