import HttpClient from "../services/HttpClient";
import axios from 'axios';
import API_ROUTES from '../config/api.routes'
import CONFIGDATA from '../config/config-data'

const axiosClient = axios.create();
axiosClient.CancelToken = axios.CancelToken;
axiosClient.isCancel = axios.isCancel;


export const loginuser = (user, dispatch) => {
  return new Promise((resolve, reject) => {
    var bodyFormData = new FormData();

    bodyFormData.set('grant_type', CONFIGDATA.grant_type);
    bodyFormData.set('client_id', CONFIGDATA.client_id);
    bodyFormData.set('client_secret', CONFIGDATA.client_secret);
    bodyFormData.set('username', user.username);
    bodyFormData.set('password', user.password);
    const headers = {
      'Content-Type': 'application/json'
    };
    axiosClient.post(API_ROUTES.LOGIN_POST, user, headers).then(response => {

      dispatch({
        type: "LOGIN_SESSION",
        session: response.data,
        authenticated: true
      })

      resolve(response);
    }).catch(error => {
      if (typeof error.response === 'undefined') {
        console.log('A network error occurred. '
          + 'This could be a CORS issue or a dropped internet connection. '
          + 'It is not possible for us to know.')
        reject({ error: 'technical_error', error_description: 'Occurió un porblema de CORS comuniquese con el administrador.' });
      }

      reject(error.response);
    });
  //   let response = {
  //     status: 200,
  //     data: {
  //       access_token: "fsdfsdfsfsfsdfsfsfsfdsfsf",
  //       expires_in: 15655,
  //       refresh_expires_in: 154154,
  //       refresh_token: "sdsdsdsdsdsdsdsd",
  //       token_type: "bearer",
  //       "not-before-policy": 55550,
  //       session_state: "dsds",
  //       scope: "dsdsdsdsdsdsds"

  //     }
  //   };

  //   dispatch({
  //     type: "LOGIN_SESSION",
  //     session: response.data,
  //     authenticated: true
  //   });
  //   resolve(response);
  // });
  });
}
