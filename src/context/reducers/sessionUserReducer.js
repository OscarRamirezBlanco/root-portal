import {saveState} from '../../utils/localStorage';

export const initialState = {
    user: {
        nombres: '',
        apellidos: '',
        email: '',
        telefono: '',
        tipoDocumentoId: '',
        numeroCedula: '',
        ciudadId: '',
        imagen: ''
    },
    authenticated: false,
    token: {
        access_token: "",
        expires_in: 0,
        refresh_expires_in: 0,
        refresh_token: "",
        token_type: "bearer",
        "not-before-policy": 0,
        session_state: "",
        scope: ""
    },

}

const sessionUserReducer = (state = initialState, action) => {
    saveState( {
        ...state,
        token: action.session,
        authenticated: action.authenticated
    });
    switch (action.type) {
        case "LOGIN_SESSION":
            return {
                ...state,
                token: action.session,
                authenticated: action.authenticated
            };
            case "LOGOUT":
                return {
                    ...state,
                    token: action.session,
                    authenticated: action.authenticated
                };
            
        default: return state;
    }

    
};

export default sessionUserReducer;